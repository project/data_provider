<?php

declare(strict_types=1);

namespace Drupal\data_provider\Contracts;

/**
 * Define the data provider transformer manager interface.
 */
interface DataProviderTransformerManagerInterface extends DataProviderDefaultPluginManagerInterface {}
